// components/lottery-record.js

import {
  queryLotteryRecords
} from '../api/user.js'

Component({
  /**
   * 组件的属性列表
   */
  properties: {
    type: {
      type: String,
      value: 'awarded-lottery'
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    currentPage: 1,
    pageSize: 10,
    totalPage: 1,
    noRecords: false,
    noMoreRecords: false,
    querying: false,
    records: []
  },

  ready() {

    this.onQueryLotteryRecords();

  },

  /**
   * 组件的方法列表
   */
  methods: {

    onQueryLotteryRecords() {

      this.data.querying = true;
      queryLotteryRecords(this.properties.type, this.data.currentPage, this.data.pageSize).then(res => {

        this.data.querying = false;
        if (res.result.errMsg === 'lottery-record-page:none') {
          this.setData({
            noRecords: true,
            noMoreRecords: true,
            currentPage: res.result.data.currentPage,
            totalPage: res.result.data.totalPage,
            records: []
          });
          return;
        }
        this.setData({
          noRecords: false,
          currentPage: res.result.data.currentPage,
          records: res.result.data.records,
          totalPage: res.result.data.totalPage,
          noMoreRecords: res.result.data.totalPage <= res.result.data.currentPage
        });

      });

    },

    onPullDownRefresh() {

      wx.showNavigationBarLoading();
      this.data.currentPage = 1;
      this.onQueryLotteryRecords();
      setTimeout(() => {
        wx.hideNavigationBarLoading();
        wx.stopPullDownRefresh();
      }, 1000);

    },

    onReachBottom() {

      if (this.data.noMoreRecords) return;
      if (this.data.querying) return;

      this.queryMoreLotteryRecords();

    },

    queryMoreLotteryRecords() {

      if (this.data.currentPage >= this.data.totalPage) {
        return;
      }
      this.data.currentPage++;
      this.data.querying = true;
      wx.showNavigationBarLoading();
      queryLotteryRecords(this.properties.type, this.data.currentPage, this.data.pageSize).then(res => {
        wx.hideNavigationBarLoading();
        this.data.querying = false;
        if (res.result.errMsg === 'lottery-record-page:none') {
          this.setData({
            noRecords: true,
            noMoreRecords: true,
            currentPage: res.result.data.currentPage,
            totalPage: res.result.data.totalPage,
            records: []
          });
          return;
        }
        if (res.result.errMsg === 'lottery-record-page:empty') {
          this.setData({
            currentPage: res.result.data.currentPage,
            totalPage: res.result.data.totalPage,
            noMoreRecords: true
          });
          return;
        }
        this.setData({
          noRecords: false,
          currentPage: res.result.data.currentPage,
          records: this.data.records.concat(res.result.data.records),
          totalPage: res.result.data.totalPage,
          noMoreRecords: res.result.data.totalPage <= res.result.data.currentPage
        });
      });

    }

  }

})