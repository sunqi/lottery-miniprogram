// miniprogram/pages/index/index.js

import {
  queryLotteries
} from '../../api/user.js'

import {
  queryBroadcasts
} from '../../api/broadcast.js'

Page({

  /**
   * 页面的初始数据
   */
  data: {

    broadcastIndex: 0, // 数据索引

    swiperIndex: 0, // swiper索引
    swiperInterval: 5000,
    swiperDuration: 500,
    defaultBroadcast: {
      type: 'text',
      text: '永远相信美好的事情即将发生'
    },
    broadcasts: [],
    broadcastIndexes: [],
    broadcastCount: 0,

    lotteries: [],
    currentPage: 1,
    pageSize: 10,
    totalPage: 1,
    noLotteries: false,
    noMoreLotteries: false,
    querying: false

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    console.log(decodeURIComponent(options.scene))

    this.data.broadcastCount = 1;
    this.setData({
      broadcasts: [this.data.defaultBroadcast]
    });
    this.onQueryBroadcasts();

    if (options.lotteryId) {
      wx.navigateTo({
        url: '/pages/participate/index?lotteryId=' + options.lotteryId
      });
    }

  },

  onQueryBroadcasts() {

    queryBroadcasts().then(res => {

      if (res.result.errMsg === 'query.broadcasts:ok') {
        res.result.data.broadcasts.unshift(this.data.defaultBroadcast);
        this.data.broadcastCount = res.result.data.broadcasts.length;
        if (this.data.broadcastCount < 3) {
          this.setData({
            broadcasts: res.result.data.broadcasts
          });
          return;
        }
        this.setData({
          broadcastIndex: 0,
          swiperIndex: 0,
          broadcastIndexes: [0, 1, 2],
          broadcasts: res.result.data.broadcasts
        });
      }

    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

    this.onQueryLotteries();

  },

  onQueryLotteries() {

    this.data.querying = true;
    queryLotteries(this.data.currentPage, this.data.pageSize).then(res => {
      this.data.querying = false;
      if (res.result.errMsg === 'lottery-query-page:none') {
        this.setData({
          noLotteries: true,
          noMoreLotteries: true,
          currentPage: res.result.data.currentPage,
          totalPage: res.result.data.totalPage,
          lotteries: []
        });
        return;
      }
      this.setData({
        noLotteries: false,
        currentPage: res.result.data.currentPage,
        totalPage: res.result.data.totalPage,
        noMoreLotteries: res.result.data.totalPage <= res.result.data.currentPage,
        lotteries: res.result.data.lotteries
      });
    }).catch(error => {
      console.log(error)
    });

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
    this.onQueryLotteries();

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

    wx.showNavigationBarLoading();
    this.data.currentPage = 1;
    this.onQueryLotteries();
    this.onQueryBroadcasts();
    setTimeout(() => {
      wx.hideNavigationBarLoading();
      wx.stopPullDownRefresh();
    }, 1000);

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

    if (this.data.noMoreLotteries) return;
    if (this.data.querying) return;

    this.queryMoreLotteries();

  },

  queryMoreLotteries() {

    if (this.data.currentPage >= this.data.totalPage) {
      return;
    }
    this.data.currentPage++;
    this.data.querying = true;
    wx.showNavigationBarLoading();
    queryLotteries(this.data.currentPage, this.data.pageSize).then(res => {
      wx.hideNavigationBarLoading();
      this.data.querying = false;
      if (res.result.errMsg === 'lottery-query-page:none') {
        this.setData({
          noLotteries: true,
          noMoreLotteries: true,
          currentPage: res.result.data.currentPage,
          totalPage: res.result.data.totalPage,
          lotteries: []
        });
        return;
      }
      if (res.result.errMsg === 'lottery-query-page:empty') {
        this.setData({
          currentPage: res.result.data.currentPage,
          totalPage: res.result.data.totalPage,
          noMoreLotteries: true
        });
        return;
      }
      this.setData({
        noLotteries: false,
        currentPage: res.result.data.currentPage,
        lotteries: this.data.lotteries.concat(res.result.data.lotteries),
        totalPage: res.result.data.totalPage,
        noMoreLotteries: res.result.data.totalPage <= res.result.data.currentPage
      });
    });

  },

  onSwiperChange(e) {

    if (e.detail.source !== 'autoplay') return;
    //更新swiper索引
    let current = e.detail.current;
    let last = this.data.swiperIndex;
    this.data.swiperIndex = current;

    //更新数据索引
    if (current === last + 1 || last + 1 === this.data.broadcastIndexes.length + current) {
      //正向
      this.data.broadcastIndex++;
    } else {
      //逆向
      this.data.broadcastIndex--;
    }

    //数据索引边界处理
    if (this.data.broadcastIndex > this.data.broadcastCount - 1) {
      this.data.broadcastIndex = 0;
    } else if (this.data.broadcastIndex < 0) {
      this.data.broadcastIndex = this.data.broadcastCount - 1;
    }

    //确定三个item的数据值
    let next = this.data.broadcastIndex === this.data.broadcastCount - 1 ? 0 : this.data.broadcastIndex + 1;
    let pre = this.data.broadcastIndex === 0 ? this.data.broadcastCount - 1 : this.data.broadcastIndex - 1;

    //当前item不用处理，其他两个item更新显示；
    if (this.data.swiperIndex === 0) {
      this.data.broadcastIndexes[1] = next;
      this.data.broadcastIndexes[2] = pre;
    } else if (this.data.swiperIndex === 1) {
      this.data.broadcastIndexes[0] = pre;
      this.data.broadcastIndexes[2] = next;
    } else if (this.data.swiperIndex === 2) {
      this.data.broadcastIndexes[0] = next;
      this.data.broadcastIndexes[1] = pre;
    }
    this.setData({
      broadcastIndexes: this.data.broadcastIndexes
    });

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }

})