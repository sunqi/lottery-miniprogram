// miniprogram/pages/my/index.js

import {
  queryLotteryRecordCounts
} from '../../api/user.js'

const app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    counts: {
      'awarded-lottery': 0,
      'total-lottery': 0,
      'waiting-lottery': 0
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

    this.refresh();

  },

  refresh() {

    queryLotteryRecordCounts().then(res => {

      if (res.result.errMsg === "query-lottery-record-counts:ok") {
        this.setData({
          counts: res.result.data.counts
        });
      }
      
    });

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

    setTimeout(() => {
      wx.stopPullDownRefresh();
    }, 1000);

    this.refresh();

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }

})